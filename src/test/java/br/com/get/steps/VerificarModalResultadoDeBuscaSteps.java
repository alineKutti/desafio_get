package br.com.get.steps;

import br.com.get.pages.ModalInformacoesPage;
import br.com.get.pages.PrincipalPage;
import br.com.get.pages.ResultadoBuscaPage;
import br.com.get.suport.Web;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import org.junit.After;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

public class VerificarModalResultadoDeBuscaSteps {

	private WebDriver navegador;

	@Dado("^que acessei o site: https://site\\.getnet\\.com\\.br/$")
	public void queAcesseiOSiteHttpsSiteGetnetComBr() {
		navegador = Web.createChrome();
	}

	@Dado("^Cliquei no botao de busca$")
	public void cliqueiNoBotaoDeBusca() throws InterruptedException {
		new PrincipalPage(navegador).clicarNaBusca();
	}

	@Dado("^informei no campo de busca\\(LUPA\\) o valor  \"([^\"]*)\"$")
	public void informeiNoCampoDeBuscaLupaOValor(String informacaoBusca) {
		new PrincipalPage(navegador).informaBusca(informacaoBusca);
	}

	@Dado("^executo a pesquisa$")
	public void executoAPesquisa() {
		new PrincipalPage(navegador).executaPesquisa();
	}

	@Dado("^retorna a lista de resultados encontrados \"([^\"]*)\"$")
	public void retornaAListaDeResultadosEncontrados(String resultadoEsperado) {
		String resultadoBusca = new ResultadoBuscaPage(navegador).validatextoResultadoBusca();
		Assert.assertEquals(resultadoEsperado, resultadoBusca);
	}

	@Dado("^localizei o link \"([^\"]*)\"$")
	public void localizeiOLink(String nomeLinkEsperado) {
		String infoLinkRetornado = new ResultadoBuscaPage(navegador).retonarInformacaoLink();
		Assert.assertEquals(nomeLinkEsperado, infoLinkRetornado);
	}

	@Quando("^clicar no link$")
	public void clicarNoLink() {

		new ResultadoBuscaPage(navegador).clicarLinkBusca();
	}

	@Entao("^a modal foi aberta com a mensagem \"([^\"]*)\"$")
	public void aModalFoiAbertaComAMensagem(String mensagemEsperada) {
		String mensagemRetornada = new ModalInformacoesPage(navegador).retornaInformacaoTituloModal();
		Assert.assertEquals(mensagemEsperada, mensagemRetornada);
	}

	@Entao("^clica no botao de fechar modal$")
	public void clicaNoBotaoDeFecharModal() {

		new ModalInformacoesPage(navegador).fecharmodal();
	}

	@After
	public void finaliza() {
		if (navegador != null) {
			navegador.quit();
		}
	}
}
