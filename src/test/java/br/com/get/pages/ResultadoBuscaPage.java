package br.com.get.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ResultadoBuscaPage extends BasePage {
	
	public ResultadoBuscaPage(WebDriver navegador) {
		super(navegador);
	}

	public  String validatextoResultadoBusca() {
		String resultadoBusca = navegador.findElement(By.className("c-search-page__title")).getText();
		return resultadoBusca;
	}

	public String retonarInformacaoLink(){
		navegador.findElement(By.cssSelector("body > div.c-wrapper > div > section > a:nth-child(3) > h3")).isDisplayed();
		String infoLinkRetornado = navegador.findElement(By.cssSelector("body > div.c-wrapper > div > section > a:nth-child(3) > h3")).getText();
		return infoLinkRetornado;
	}

	public ModalInformacoesPage clicarLinkBusca() {
		navegador.findElement(By.cssSelector("body > div.c-wrapper > div > section > a:nth-child(3) > h3")).click();
		return new ModalInformacoesPage(navegador);
	}
}
