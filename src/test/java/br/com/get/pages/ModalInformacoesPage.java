package br.com.get.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ModalInformacoesPage extends BasePage{

    public ModalInformacoesPage(WebDriver navegador) {
        super(navegador);
    }

    public String retornaInformacaoTituloModal(){
        navegador.findElement(By.xpath("/html/body/div[20]/div/div[@class='o-modal__title']")).isDisplayed();
        String mensagemRetornada = navegador.findElement(By.xpath("/html/body/div[20]/div/div[@class='o-modal__title']")).getText();

        return mensagemRetornada;
    }

    public  ModalInformacoesPage fecharmodal() {
        navegador.findElement(By.cssSelector("body > div.o-modal.is-modal-open > div > div.o-modal__close.o-modal__close-x")).click();
        return new ModalInformacoesPage(navegador);
    }

}
