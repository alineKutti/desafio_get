package br.com.get.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class PrincipalPage extends BasePage {

	public PrincipalPage(WebDriver navegador) {
		super(navegador);
	}

	public PrincipalPage clicarNaBusca() throws InterruptedException {
		Thread.sleep(10000);
		navegador.findElement(By.id("search-trigger")).click();

		return new PrincipalPage(navegador);
	}

	public  PrincipalPage informaBusca(String informacaoPesquisa) {
		navegador.findElement(By.id("global-search-input")).sendKeys(informacaoPesquisa);

		return new PrincipalPage(navegador);
	}

	public  ResultadoBuscaPage executaPesquisa() {
		navegador.findElement(By.xpath("/html/body/section/div/div/div/form/button")).click();
		
		return new ResultadoBuscaPage(navegador);
	}
}
