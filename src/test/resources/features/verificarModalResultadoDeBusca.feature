# language : pt
Funcionalidade: Pesquisar e verificar resultado de busca clicando em um link
	Como um usuario
	Eu quero pesquisar por "superget"
	para que eu possa clicar no link "Como acesso a minha conta SuperGet?"
	E verificar se a modal foi aberta com a mensagem "Como acesso a minha conta SuperGet?"

Cenario: Deve executar a pesquisa no campo de busca e verificar se a modal aberta apresenta a mensagem correta
	Dado  que acessei o site: https://site.getnet.com.br/
	E Cliquei no botao de busca
	E informei no campo de busca(LUPA) o valor  "superget"
	E  executo a pesquisa
	E  retorna a lista de resultados encontrados "Resultados da busca para: superget"
	E localizei o link "Como acesso a minha conta SuperGet?"
	Quando  clicar no link
	Entao  a modal foi aberta com a mensagem "Como acesso a minha conta SuperGet?"
	E clica no botao de fechar modal


